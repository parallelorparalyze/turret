/*
 Arizona State University
 Capstone Fall 2011/Spring 2012
 Team: Purple Threads - Nick Moulin, Gabe Silva, Nadim Hoque, Craig Hartmann, Anthony Russo, Duc Tran
 Sponsor: Dr. Avrial Shrivastava

 Description: This is the main program to control the operation of the turret system for our Computer System Engineering Capstone project
 The program is to take RGB and Depth data from a kinect sensor,
 detect a moving orange object to be the target,
 calculate where the object will be at time t,
 calculate the angle of rotation for the servos to shoot a dart at the object at time t,
 send the commands to move the servos,
 calculate if we have a high probability of hitting the target,
 and send the command to fire the dart at the target.

 The program is also to simulate the penalty that would be incurred if the program was run on a single core machine and used a object detection algorithm that had a larger CPU penalty

 un-parallelized Cascade Classifier object detection takes on average 600ms
 parallelized the Cascade Classifier can achieve a speed up of about 24X.

 For our simulation a blob detection algoritm simulates the time it would take a parallelized cascade classifier to calculate
 and a 600ms penalty is added if we wish to demonstrate the functionality in a un-parallelized state.

 */

#include "Turret.h"
#include <stdlib.h>
#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <libfreenect.h>
#include <libfreenect_sync.h>
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>


#define DEFAULT_BAUDRATE 9600
#define DEFAULT_SERIALPORT "/dev/ttyUSB0"
#define VERBOSE if(0) //Print Extra Debug Info. Default Info is Only FPS

int rdyToFire = 0; //Track the number of positive Locks from the firing solution
double timer = 2; //The time it takes the turret to fire and reload. The fastest possible turnaround time
int ammo = 40000; //Number of Darts the turret has
int SINGLECORE = 0;

//Global data for use with serial

int ucIndex; //ucontroller index number

using namespace std;

// Convert the depth desparity matrix held in the depth image into a very basic image
// the closer the object the closer to the color red it will be.
IplImage *GlViewColor(IplImage *depth) {
	static IplImage *image = 0;
	if (!image)
		image = cvCreateImage(cvSize(640, 480), 8, 3);
	unsigned char *depth_mid = (unsigned char*) (image->imageData);
	int i;

	for (i = 0; i < 640 * 480; i++) {
		int lb = ((short *) depth->imageData)[i] % 256;
		int ub = ((short *) depth->imageData)[i] / 256;
		switch (ub) {
		case 0:
			depth_mid[3 * i + 2] = 255;
			depth_mid[3 * i + 1] = 255 - lb;
			depth_mid[3 * i + 0] = 255 - lb;
			break;
		case 1:
			depth_mid[3 * i + 2] = 255;
			depth_mid[3 * i + 1] = lb;
			depth_mid[3 * i + 0] = 0;
			break;
		case 2:
			depth_mid[3 * i + 2] = 255 - lb;
			depth_mid[3 * i + 1] = 255;
			depth_mid[3 * i + 0] = 0;
			break;
		case 3:
			depth_mid[3 * i + 2] = 0;
			depth_mid[3 * i + 1] = 255;
			depth_mid[3 * i + 0] = lb;
			break;
		case 4:
			depth_mid[3 * i + 2] = 0;
			depth_mid[3 * i + 1] = 255 - lb;
			depth_mid[3 * i + 0] = 255;
			break;
		case 5:
			depth_mid[3 * i + 2] = 0;
			depth_mid[3 * i + 1] = 0;
			depth_mid[3 * i + 0] = 255 - lb;
			break;
		default:
			depth_mid[3 * i + 2] = 0;
			depth_mid[3 * i + 1] = 0;
			depth_mid[3 * i + 0] = 0;
			break;
		}
	}
	return image;
}





int main(int argc, char **argv) {
	setbuf(stdout, NULL);
	char port[20]; //port name
	int baud; //baud rate
	char topicSubscribe[20];
	char topicPublish[20];

	if (argc > 1) {
		if (sscanf(argv[1], "%d", &ucIndex) == 1) {
			sprintf(topicSubscribe, "uc%dCommand", ucIndex);
			sprintf(topicPublish, "uc%dResponse", ucIndex);
		} else {
			exit(EXIT_FAILURE);
		}
	} else {
		strcpy(topicSubscribe, "uc0Command");
		strcpy(topicPublish, "uc0Response");
	}

	strcpy(port, DEFAULT_SERIALPORT);
	if (argc > 2)
		strcpy(port, argv[2]);

	baud = DEFAULT_BAUDRATE;
	if (argc > 3) {
		if (sscanf(argv[3], "%d", &baud) != 1) {
			printf("ucontroller baud rate parameter invalid\n");
			exit(EXIT_FAILURE);
		}
	}

	Turret theTurret = Turret(port, baud);

	cvSize(640, 480); //Specifiy the Size of the Image
	freenect_sync_set_tilt_degs(1, 0); //Set the Kinect to the default angle

	//Setup Windows to view Images
	cvNamedWindow("Camera", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Car", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Depth", CV_WINDOW_AUTOSIZE);

	CvKalman* kalman = cvCreateKalman(4, 2, 0);

	//Create Initial transitionMatrix
	//CV_32FC1 means 32 floating point type

	CvMat* x_k = cvCreateMat(4, 1, CV_32FC1);
	float initVals[] = { 0, 0, 0, 0 };
	memcpy(x_k->data.fl, initVals, sizeof(initVals));

	//measurements two parameters x and y
	CvMat* z_k = cvCreateMat(2, 1, CV_32FC1);
	cvZero(z_k);

	//Transition Matrix F
	const float F[] = { 1, 0, 10, 0, 0, 1, 0, 10, 0, 0, 1, 0, 0, 0, 0, 1 };

	memcpy(kalman->transition_matrix->data.fl, F, sizeof(F));

	//Measurement matrix set to identity matrix
	//process noise set to low value
	//measurement noise set to value
	//error_conv set to identity

	cvSetIdentity(kalman->measurement_matrix, cvRealScalar(1));
	cvSetIdentity(kalman->process_noise_cov, cvRealScalar(1e-5));
	cvSetIdentity(kalman->measurement_noise_cov, cvRealScalar(1e-1));
	cvSetIdentity(kalman->error_cov_post, cvRealScalar(1));

	//initial state needs to be set
	kalman->state_pre->data.fl[0] = 95;
	kalman->state_pre->data.fl[0] = 145;
	kalman->state_pre->data.fl[2] = 0;
	kalman->state_pre->data.fl[3] = 0;

	CvScalar yellow = CV_RGB(255,255,0);
	CvScalar white = CV_RGB(255,255,255);
	CvScalar redColor = CV_RGB(255,0,0);


	clock_t previous_t = clock(), current_t = clock(); //To track the time between each iteration of the while loop

	while (1) {

		//Get RGB and Depth Data from Kinect
		angles cordinates;
		char *data;
		char *datadepth;
		IplImage *image = cvCreateImageHeader(cvSize(640, 480), 8, 3);
		IplImage *depthImage = cvCreateImageHeader(cvSize(640, 480), 16, 1);

		CvScalar s;
		uint32_t timestamp;
		freenect_sync_get_video((void**) (&data), &timestamp, 0,
				FREENECT_VIDEO_RGB);
		freenect_sync_get_depth((void**) (&datadepth), &timestamp, 0,
				FREENECT_DEPTH_11BIT);
		cvSetData(depthImage, datadepth, 640 * 2);
		cvSetData(image, data, 640 * 3);
		cvCvtColor(image, image, CV_RGB2BGR);

		//Update the timers
		previous_t = current_t;
		current_t = clock();

		//Calculate how much time the last iteration took
		double deltaTime = current_t - previous_t;
		deltaTime = deltaTime / CLOCKS_PER_SEC;
		VERBOSE
			printf("DeltaTime %.21f\n", deltaTime);

		//Calculate and report the FPS, Each iteration processes one frame.
		double fps = 1 / deltaTime;
		printf("FPS: %.4f\n", fps);

		//Update the refire timer for the turret.
		//The turret cannot fire until the refire timer is 0
		if (timer > 0) {
			timer = timer - deltaTime;
		} else {
			timer = 0;
		}
		VERBOSE
			printf("Timer %.4f\n", timer);

		//Set up frames to object detect
		IplImage* hsv_frame = cvCreateImage(cvGetSize(image), 8, 3);
		IplImage* red = cvCreateImage(cvGetSize(image), 8, 1);
		IplImage* sat = cvCreateImage(cvGetSize(image), 8, 1);
		IplImage* car = cvCreateImage(cvGetSize(image), 8, 1);

		//Convert the RGB image to a HSV image so we can split out the saturation values
		cvCvtColor(image, hsv_frame, CV_BGR2HSV);
		//grab the saturation from sv_image
		cvSplit(hsv_frame, NULL, sat, NULL, NULL);

		//split BGR image into red only
		cvSplit(image, NULL, NULL, red, NULL);

		//filter out low red and low saturation
		cvThreshold(red, red, 128, 255, CV_THRESH_BINARY);
		cvThreshold(sat, sat, 128, 255, CV_THRESH_BINARY);

		//combine images
		cvMul(red, sat, car);

		//Erode out very small positive detections
		cvErode(car, car, NULL, 2);
		//Increase the size of the positive detections still left
		cvDilate(car, car, NULL, 5);

		//ASSUMPTION OF ONLY ONE COLORED OBJECT ON SCREEN
		//moments are used to compare two contours
		//a countour is a series of pixels that describe a curve or division of an object
		//can be done with a pos/neg threshold or a cvCanny

		CvMoments *moments = (CvMoments*) malloc(sizeof(CvMoments));
		cvMoments(car, moments, 1);

		double moment10 = cvGetSpatialMoment(moments, 1, 0);
		double moment01 = cvGetSpatialMoment(moments, 0, 1);
		double area = cvGetCentralMoment(moments, 0, 0);

		//Dividing moment10 by area gives x-cord
		//Dividing moment01 by area gives y-cord

		static int posX = 0;
		static int posY = 0;

		int lastX = posX;
		int lastY = posY;

		//Protect against div-by-zero
		if (area != 0) {
			posX = moment10 / area;
			posY = moment01 / area;

			//find the difference between the last frame and current frame
			int deltaX = posX - lastX;
			int deltaY = posY - lastY;

			VERBOSE
				printf("Position X = %d\n", posX);
			VERBOSE
				printf("Position Y = %d\n", posY);

			//PosX and PosY sometimes report very huge and very small values, filter them out
			if (deltaTime != 0 && (40 < posX) && (posX < 640) && (posY < 480)) {
				//Calculate the slow at this exact moment in time based off of the difference between this image and the previous image
				double slopeX = ((double) deltaX / deltaTime);
				double slopeY = ((double) deltaY / deltaTime);
				s = cvGet2D(depthImage, posY, posX); // get the (i,j) pixel value for depth
				if (s.val[0] != 2047) //if depth value is not recorded a value of 2047 is stored in the matrix
						{
					const CvMat* y_k = cvKalmanPredict(kalman, 0); //get the current prediction of the next frame

					//Generate Measurement
					float vals[] = { (float) posX, (float) posY };
					memcpy(z_k->data.fl, vals, sizeof(vals));

					//Yellow Circle is the current position
					cvCircle(image, cvPoint(posX, posY), 4, yellow, -1, 4);
					// White is the predicted position via the filter
					cvCircle(image, cvPoint(y_k->data.fl[0], y_k->data.fl[1]),
							4, white, -1, 4);

					//Predict where the object will be at time t
					//get velocity from kalman for x and y
					// velocity * time = new pos
					// draw 0 at new pos

					double estimatedPosX = y_k->data.fl[2] * theTurret.timeToTarget
							+ y_k->data.fl[0];
					double estimatedPosY = y_k->data.fl[3] * theTurret.timeToTarget
							+ y_k->data.fl[1];

					//Red Circle is where we predict the object will be at time t
					cvCircle(image, cvPoint(estimatedPosX, estimatedPosY), 4,
							redColor, -1, 4);

					//add data from the current frame to the kalman
					cvKalmanCorrect(kalman, z_k);

					//Calculate the percent error between where we think the object will be and where it currently is
					//This will filter out extremely fast moving objects and quick changes in direction
					double percentErrorX = (abs(y_k->data.fl[0] - posX))
							/ (double) (posX);
					double percentErrorY = (abs(y_k->data.fl[1] - posY))
							/ (double) (posY);

					VERBOSE
						printf("PercentErrorX: %f PercentErrorY: %f\n",
								percentErrorX, percentErrorY);

					//Get the horizontal and vertical angle based off of where we think the object will be at time t
					//Move the turret to proper angle
					cordinates = theTurret.setangle(estimatedPosX, estimatedPosY,
							s.val[0]);

					VERBOSE
						printf("Depth=%.2f\n", (s.val[0]));
					VERBOSE
						printf("Vertical: %.2f, Horizontal: %f\n",
								cordinates.phi, cordinates.theta);
					VERBOSE
						printf("The slope of X is %f \n", slopeX);
					VERBOSE
						printf("The slope of y is %f \n", slopeY);

					//Move the turret to our predicted angles


					//Only if the percent error is under 5% do we consider the kalman to be predicting correctly and we "lock" onto the target
					//If we have one "no-lock" we must restart the locking procedure
					if ((percentErrorX < .05) && (percentErrorY < .05)) {
						rdyToFire++;
					} else {
						rdyToFire = 0;
					}

#if LIVE_FIRE

					//If we have 10 positive locks, the re-fire timer is at zero, and there is ammo in the turret
					//Then send the command to fire
					if (rdyToFire >= 10 && timer <= 0 && ammo > 0) {
						//Send the turret the command to fire
						//usb_control_msg is the method that sends the command
						printf("Fire Turret\n");
						theTurret.fire();
						//Decrease the amount of ammo
						ammo = ammo - 1;
						//Reset the refire timer
						timer = 2;
					}
#endif
				}
			}

		}

		//Show the image to the screen
		cvShowImage("Camera", image);
		cvShowImage("Depth", GlViewColor(depthImage));
		cvShowImage("Car", car);

		// Wait for a keypress for 10us
		int c = cvWaitKey(10);
		// -1 is returned if no key is pressed
		if (c != -1) {
			//Manual command to fire the turret without the fireing solution
			if (c == 102) //ASCII 'f'
					{
				printf("Fire turret\n");
				theTurret.fire();

			}
			//Manual reload command
			else if (c == 114) //ASCII 'r'
					{
				printf("Reloaded\n");
				ammo = 4;
			}
			//Set the program to multithreaded mode
			else if (c == 109) // ASCII 'm'
					{
				printf("Multithread Mode\n");
				SINGLECORE = 0;
			}
			//Set the program to single core mode
			else if (c == 115) // ASCII 's'
					{
				printf("Single Core Mode\n");
				SINGLECORE = 1;
			}
			//If any other key is pressed end the program
			else {
				// If pressed, break out of the loop
				break;
			}
		}

		//Protect against memory leaks

		//cvReleaseImage(&threshold_frame);
		cvReleaseImage(&hsv_frame);
		//free(&moments);
		cvReleaseImage(&car);
		//cvReleaseImage(&image);
		//cvReleaseImage(&depthImage);
		cvReleaseImage(&red);
		cvReleaseImage(&sat);

		//If emulating a single core system there is a 600ms penalty for object detection
		if (SINGLECORE) {
			usleep(600000);
		}
	}

	return EXIT_SUCCESS;
}
