/*
 * Turret.h
 *
 *  Created on: Oct 9, 2012
 *      Author: mike
 */

#ifndef TURRET_H_
#define TURRET_H_

#define LIVE_FIRE 1 //Turn on the Fireing solution algorithm
#define MOVE_TURRET 1 //Allow the program to send commands to the servos
#define USB_TURRET_VENDOR_ID 8483
#define VERBOSE if(0) //Print Extra Debug Info. Default Info is Only FPS


#include <termios.h>
#include <usb.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cmath>

//Global data for kinect calculations
const float gravity = 32.174; //Force of gravity in ft
const float velocity = 47.5; //Initial Velocity of the Foam Dart
const float pi = 3.14159265;
const float y_calibration = 90; //Used for quick calibration of vertical angle


struct angles {
	float theta;
	float phi;
};

class Turret {
public:
	float timeToTarget;
	Turret(char * port, int baud);
	angles setangle(float x, float y, float distance);
	void fire();
	virtual ~Turret();
private:

	usb_dev_handle* launcher;
	FILE *fpSerial; //serial port file pointer
	FILE *serialInit(char *port, int baud);
	void ucCommandCallback(const char * msg);
};

#endif /* TURRET_H_ */
