/*
 * Turret.cpp
 *
 *  Created on: Oct 9, 2012
 *      Author: mike
 */

#include "Turret.h"

Turret::Turret(char *port, int baud) {
	struct usb_bus *busses;

	usb_init();
	usb_find_busses();
	usb_find_devices();
	busses = usb_get_busses();

	struct usb_bus *bus;

	//loop through the busses to find the correct device
	for (bus = busses; bus; bus = bus->next) {
		struct usb_device *dev;
		VERBOSE
			printf("in busses loop");
		//loop through the devices on each bus
		for (dev = bus->devices; dev; dev = dev->next) {
			VERBOSE
				printf("id of Vendor: %d\n", dev->descriptor.idVendor);
			// Check if this device is the turret
			// if a new turret is used, the Vendor ID may change
			// if that is the case, then change this appropriately
			if (dev->descriptor.idVendor == USB_TURRET_VENDOR_ID) {
				launcher = usb_open(dev);
				VERBOSE
					printf("in vendorId loop\n\n");

				// if being claimed by usb_storage, need to release so we can use
				int retval;
				char dname[32] = { 0 };
				retval = usb_get_driver_np(launcher, 0, dname, 31);
				VERBOSE
					printf("retval : %d\n", retval);

				if (!retval) {
					usb_detach_kernel_driver_np(launcher, 0);
				}

				//claim device
				int claimed = usb_claim_interface(launcher, 0);
				VERBOSE
					printf("claimed : %d\n", claimed);

				if (claimed == 0) {
					printf("USB Turret Found.\n");
				} else {
					printf(
							"USB Turret Not Found. Please attach turret and try again.\n");
					exit(EXIT_FAILURE);
				}
			}
		}
	}

	printf("connection initializing (%s) at %d baud\n", port, baud);
	fpSerial = serialInit(port, baud);
	if (!fpSerial) {
		printf("unable to create a new serial port\n");
		//return 1;
	} else {
		printf("serial connection successful\n");
	}
}

Turret::~Turret() {
	//release interfaces
	usb_release_interface(launcher, 0);
	usb_close(launcher);
	printf("usb connection closed");
	fclose(fpSerial);
	printf("serial connection closed");
}

/**Initialize serial port, return file descriptor
 ** @param char * port, port number for serial
 ** @param int baud, baud rate for serial
 ** @return FILE* as handle for serial connection
 **/

FILE *Turret::serialInit(char * port, int baud) {
	int BAUD;
	//Look up appropriate baud rate constant
	switch (baud) {
	case 38400:
	default:
		BAUD = B38400;
		break;
	case 19200:
		BAUD = B19200;
		break;
	case 9600:
		BAUD = B9600;
		break;
	case 4800:
		BAUD = B4800;
		break;
	case 2400:
		BAUD = B2400;
		break;
	case 1800:
		BAUD = B1800;
		break;
	case 1200:
		BAUD = B1200;
		break;
	}
	int fd = -1;
	struct termios newtio;
	FILE *fp = NULL;

	//Open the serial port as a file descriptor for low level configuration
	// read/write, not controlling terminal for process,
	fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0) {
		printf("serialInit: Could not open serial device %s", port);
		return fp;
	}

	// set up new settings
	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag = CS8 | CLOCAL | CREAD; //no parity, 1 stop bit

	newtio.c_iflag = IGNCR; //ignore CR, other options off
	newtio.c_iflag |= IGNBRK; //ignore break condition

	newtio.c_oflag = 0; //all options off
	newtio.c_lflag = ICANON; //process input as lines

	// activate new settings
	tcflush(fd, TCIFLUSH);

	if (cfsetispeed(&newtio, BAUD) < 0 || cfsetospeed(&newtio, BAUD) < 0) {
		printf("seralInit: Failed to set serial baud rate: %d", baud);
		close(fd);
		return NULL;
	}

	tcsetattr(fd, TCSANOW, &newtio);
	tcflush(fd, TCIOFLUSH);

	//Open file as a standard I/O stream
	fp = fdopen(fd, "r+");
	if (!fp) {
		printf("serialInit: Failed to open serial stream %s", port);
		fp = NULL;
	}
	return fp;
} //end of serialInit

void Turret::fire() {
	static char fire_bytes[] = {0x02, 0x10, 0, 0, 0, 0, 0, 0};
	usb_control_msg(launcher, 0x221, 0x09, 0x0200, 0,
			 fire_bytes, 8, 1000);
}

/**Process command message, send to uController
 ** @param const char* msg - will be in the format of
 ** 'E (char)' or 'B (char)' to send over serial
 **/
void Turret::ucCommandCallback(const char * msg) {
	VERBOSE
		printf("in CommandCall: '%s'\n", msg);
	fprintf(fpSerial, "%s", msg); //appends newline
	fflush(fpSerial);
} //ucCommandCallback

//setangle calculates the vertical and horizontal rotation of the servos based off the x, y, and distance values passed in.
// X value is a pixel point on the x-axis of an openCV IplImage.
// Y value is a pixel point on the y-axis of an openCV IplImage.
// The distance value is the depth disparity value returned by the Kinect Sensor
// For all calculations we assume the RGB camera and Turret is located at the center of each IplImage at point (320,240)
// The Origin of an IplImage is set to be in the upper left of the Image
// The Horizontal Servo is designed such that 90 degrees is the halfway point of rotation
// Far left position of turret 0 degrees
// Center position 90
// Far right position 180
//
// The vertical servo is designed such that 90 degrees is level with the kinect
// 0 degrees (point straight down)
// 90 degrees (horizontal)
// 180 degrees (point straight up)

angles Turret::setangle(float x, float y, float distance) {
	angles reddit;
	const char *horizontal = "B ";
	const char *vertical = "E ";

	float x_feet, d_feet, thetadeg, phideg, x_pixels, x_pixelsfoot, y_feet,
			y_pixels, y_pixelsfoot, quadSoln, angle1, angle2;

	//Two different equations exist to minimize the percent error for different depth values.

	if (distance > 950) //If Distance is greater than 9 feet
			{
		d_feet = (distance - 824.8) / 15.2;
	} else //If distance is between 1 and 9 feet
	{
		d_feet = (0.1236 * tan((distance / 2842.5) + 1.1863)) * 3.2808399;
	}

	VERBOSE
		printf("Distance in Feet: %f\n", d_feet);

	if (x < 320.00) //Point is on left side of image
			{
		x_pixels = 320.00 - x;
		x_pixelsfoot = -55.91836 * log(d_feet) + 177.49225974457;
		VERBOSE
			printf("Pixels per foot%f\n", x_pixelsfoot);
		x_feet = x_pixels / x_pixelsfoot;
		thetadeg = asin(x_feet / d_feet) * (180.0 / pi);
		thetadeg = 90 - thetadeg;
		reddit.theta = thetadeg;
	} else if (x > 320.00) //Point is on the right side of image
			{
		x_pixels = x - 320;
		x_pixelsfoot = -55.91836 * log(d_feet) + 177.49225974457;
		x_feet = x_pixels / x_pixelsfoot;
		thetadeg = asin(x_feet / d_feet) * (180.0 / pi);
		thetadeg = 90 + thetadeg;
		reddit.theta = thetadeg;
	} else if (x == 320.0) {
		x_feet = x - 320.0;
		thetadeg = 0.0;
		reddit.theta = 90.0;
	}

	if (y < 240.00) //upper half of image
			{
		y_pixels = 240 - y;
		y_pixelsfoot = -55.91836 * log(d_feet) + 177.49225974457;
		y_feet = y_pixels / y_pixelsfoot;
	} else if (y > 240.00) //lower half of image
			{
		y_pixels = y - 240;
		y_pixelsfoot = -55.91836 * log(d_feet) + 177.49225974457;
		y_feet = -1 * (y_pixels / y_pixelsfoot);
	} else {
		y_feet = 0;
	}

	quadSoln = sqrt(
			pow(velocity, 4)
					- (gravity
							* ((gravity * pow(d_feet, 2))
									+ (2 * y_feet * pow(velocity, 2)))));

	//Two possible angles are returned from the quadratic equation. We choose the smaller of the two.
	if (quadSoln == quadSoln) {
		angle1 = atan((pow(velocity, 2) + quadSoln) / (gravity * d_feet))
				* (180.0 / pi);
		angle2 = atan((pow(velocity, 2) - quadSoln) / (gravity * d_feet))
				* (180.0 / pi);
		VERBOSE
			printf("y_pixels: %f y_feet: %f\n", y_pixels, y_feet);
		VERBOSE
			printf("Angle1: %f Angle:2 %f\n", angle1, angle2);
		phideg = angle2 + y_calibration;
	} else {
		printf("Not a Number");
		phideg = 0;
	}
	reddit.phi = phideg;

	timeToTarget = d_feet / (velocity * cos(phideg)); //Determine how long it will take the dart to travel to the target

#if MOVE_TURRET
	//change the float to int then corresponding ascii char for serial
	int temp = (int) reddit.theta;
	char tempc = char(temp);
	horizontal += tempc;
	VERBOSE
		printf("-----sHorizontal: %s,    ", horizontal);
	//pass the command over the function handling communication with serial
	ucCommandCallback(horizontal);

	int vtemp = (int) reddit.phi;
	char tempv = char(vtemp);
	vertical += tempv;
	VERBOSE
		printf("-----sVertical: %s,    ", vertical);
	ucCommandCallback(vertical);
#endif
	return reddit;
}
